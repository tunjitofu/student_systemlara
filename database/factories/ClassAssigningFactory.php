<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ClassAssigning;
use Faker\Generator as Faker;

$factory->define(ClassAssigning::class, function (Faker $faker) {

    return [
        'teacher_id' => $faker->randomDigitNotNull,
        'class_schedule_id' => $faker->randomDigitNotNull,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
