<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStatusesRequest;
use App\Http\Requests\UpdateStatusesRequest;
use App\Repositories\StatusesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class StatusesController extends AppBaseController
{
    /** @var  StatusesRepository */
    private $statusesRepository;

    public function __construct(StatusesRepository $statusesRepo)
    {
        $this->statusesRepository = $statusesRepo;
    }

    /**
     * Display a listing of the Statuses.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $statuses = $this->statusesRepository->all();

        return view('statuses.index')
            ->with('statuses', $statuses);
    }

    /**
     * Show the form for creating a new Statuses.
     *
     * @return Response
     */
    public function create()
    {
        return view('statuses.create');
    }

    /**
     * Store a newly created Statuses in storage.
     *
     * @param CreateStatusesRequest $request
     *
     * @return Response
     */
    public function store(CreateStatusesRequest $request)
    {
        $input = $request->all();

        $statuses = $this->statusesRepository->create($input);

        Flash::success('Statuses saved successfully.');

        return redirect(route('statuses.index'));
    }

    /**
     * Display the specified Statuses.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $statuses = $this->statusesRepository->find($id);

        if (empty($statuses)) {
            Flash::error('Statuses not found');

            return redirect(route('statuses.index'));
        }

        return view('statuses.show')->with('statuses', $statuses);
    }

    /**
     * Show the form for editing the specified Statuses.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $statuses = $this->statusesRepository->find($id);

        if (empty($statuses)) {
            Flash::error('Statuses not found');

            return redirect(route('statuses.index'));
        }

        return view('statuses.edit')->with('statuses', $statuses);
    }

    /**
     * Update the specified Statuses in storage.
     *
     * @param int $id
     * @param UpdateStatusesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStatusesRequest $request)
    {
        $statuses = $this->statusesRepository->find($id);

        if (empty($statuses)) {
            Flash::error('Statuses not found');

            return redirect(route('statuses.index'));
        }

        $statuses = $this->statusesRepository->update($request->all(), $id);

        Flash::success('Statuses updated successfully.');

        return redirect(route('statuses.index'));
    }

    /**
     * Remove the specified Statuses from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $statuses = $this->statusesRepository->find($id);

        if (empty($statuses)) {
            Flash::error('Statuses not found');

            return redirect(route('statuses.index'));
        }

        $this->statusesRepository->delete($id);

        Flash::success('Statuses deleted successfully.');

        return redirect(route('statuses.index'));
    }
}
