<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admission;
use App\Student;
use App\Roll;
use Flash;
use Session;
use Str;
use Mail;


class StudentController extends Controller
{
    public function index()
    {
        
    }

    
    
    public function studentLogin(Request $request){
        return view('students.login'); 
    }

    public function LoginStudent(Request $request){
        if ($request->isMethod('post')) {
            $student = $request->all();
            $studentCount = Roll::where([
                'username'  => $student['username'], 
                'password'  => $student['password']
                ])
                ->count();

                if ($studentCount > 0) {
                    Session::put('studentSession', $student['username']);

                    Flash::success('Welcome' . $student['username']);

                    return redirect('/account');
                }
                else{
                    Flash::error('Your Username or Password is Incorrect');

                    return redirect('/student'); //Login Page
                }
        } 

        return view('students.account');
    }

    public function account(){
        if (Session::has('studentSession')) {
            $student = Admission::all();
            // dd($student);
        }
        else{
            return redirect('/student')->with('error','Please Login to Access');
        }

        return view('students.account',compact('student'));
    }


    public function studentBiodata(Request $request)
    {
        $students = Roll::join(
            'admissions','admissions.student_id','rolls.student_id'
            )
            ->where(['username' => Session::get('studentSession')])->first();
        return view('students.biodata', compact('students'));
    }

    public function studentChooseCourse(Request $request)
    {
        
        return view('students.lectures.choose-course');
    }

    public function studentLectureCalendar(Request $request)
    {
        
        return view('students.lectures.calendar');
    }

    public function studentLectureActivity(Request $request)
    {
        
        return view('students.lectures.activity');
    }

    public function studentExamMarks(Request $request)
    {
        
        return view('students.lectures.exam');
    }

    public function verifyPassword(Request $request)
    {
        $students = $request->all();
        $validStudent = Roll::where([
            'username' => Session::get('studentSession'),
            'password' => $students['old_password']
            ])->count();

            if($validStudent == 1)
            {
                // Flash::success('Your Username is Correct');
                echo "true"; die;
            } 
            else{
                // Flash::error('Your Username is NOT Correct');
                echo "false"; die;
            }
        return view('students.biodata', compact('students'));
    }

    public function updatePassword(Request $request)
    {
        $student_req = $request->all();
        $students = Admission::where('email', $student_req['email'])->first();
        // dd($students);

        $studentCount = Roll::where([
            'username' => Session::get('studentSession'),
            'password' => $student_req['old_password']

        ])->count();
        if ($studentCount == 1) {
            //Valid Password send message and update password
            $new_password = $student_req['new_password'];

            Roll::where('username' , Session::get('studentSession'))
                ->update(['password' => $new_password]);
            
            Flash::success('Password Changed');
            return redirect()->back();

        } else {
                //send invalid password message
                Flash::error('Password NOT Changed');
                return redirect()->back();
        }
        
    }


    public function getForgotPassword()//display form for the forgot password
    {
        return view('students.forgot-password');
    }

    public function ForgotPassword(Request $request)//send mail
    {
        $data = $request->all();
        $studentCount = Admission::where('email',$data['email'])->count();
        if ($studentCount == 0) {
            Flash::error('Email Not Found');
            return redirect()->back();
        }
        Session::put('studentSession',$data['username']);
        $students = Admission::where('email', $data['email'])->first();
        $new_rand_password = Str::random(12);
        // dd($new_rand_password);

        Roll::where('username',Session::get('studentSession'))
                ->update(['password'=> $new_rand_password]);

        $email = $data['email'];
        $student_name = $students->fname;
        $message = [
            'email'=> $email,
            'fname'=> $student_name,
            'password'=> $new_rand_password
        ];
        // dd($new_rand_password);

        Mail::send(
                    'emails.forgot-password', 
                    $message, 
                    function($message) use ($email){
                        $message->to($email)->subject('Reset Password - Academic Info Sys');

                    }
                );
        Flash::success('We have e-mailed your password reset link to '. $data['email']);
        return redirect()->back();

    }

    public function studentLogout()
    {
        Session::flush();
        Flash::success('Logged out Successfully');
        return redirect('/');
    }

    public function languages($locale)
    {
        Session()->put('locale', $locale);
        return redirect()->back();
    }

}
