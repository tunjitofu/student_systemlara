<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAdmissionRequest;
use App\Http\Requests\UpdateAdmissionRequest;
use App\Repositories\AdmissionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\Admission;
use App\Models\Department;
use App\Models\Faculty;
use App\Models\Teacher;
use App\Models\Batch;
use App\Roll;
use Auth;
use Illuminate\Support\Str;

class AdmissionController extends AppBaseController
{
    /** @var  AdmissionRepository */
    private $admissionRepository;

    public function __construct(AdmissionRepository $admissionRepo)
    {
        $this->admissionRepository = $admissionRepo;
    }

    /**
     * Display a listing of the Admission.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $student_id = Admission::max('student_id');
        $roll_id = Roll::max('roll_id');
        $teacher_id = Teacher::all();
        $faculties = Faculty::all();
        $departments = Department::all();
        $batches = Batch::all();
        $admissions = Admission::join('faculties','faculties.faculty_id','admissions.faculty_id')
                                ->join('departments','departments.department_id','admissions.department_id')
                                ->join('batches','batches.batch_id','admissions.batch_id')
                                ->get();


        $username = 'SN-'.date('Y').'-'.($student_id+1);
        $password = strtoupper(Str::random(3)).mt_rand(10, 99).strtoupper(Str::random(3));

        return view('admissions.index', compact('teacher_id','roll_id','admissions','student_id','faculties','departments','batches','username','password'))
            ->with('admissions', $admissions);
    }

    /**
     * Show the form for creating a new Admission.
     *
     * @return Response
     */
    public function create()
    {
        return view('admissions.create');
    }

    /**
     * Store a newly created Admission in storage.
     *
     * @param CreateAdmissionRequest $request
     *
     * @return Response
     */
    public function store(CreateAdmissionRequest $request)
    {
        $input = $request->all();

        $file = $request->file('image');
        $extension = $file->getClientOriginalExtension();
        $new_img_name = $request->lname.'_'.time().'.'.$extension;
        $file->move(public_path('student_images'),$new_img_name);

        $student = new Admission;
        $student->fname = $request->fname;
        $student->lname = strtoupper($request->lname);
        $student->father_name = $request->father_name;
        $student->father_phone = $request->father_phone;
        $student->mother_name = $request->mother_name;
        $student->gender = $request->gender;
        $student->email = $request->email;
        $student->dob = $request->dob;
        $student->phone = $request->phone;
        $student->address = $request->address;
        $student->current_address = $request->current_address;
        $student->nationality = $request->nationality;
        $student->passport = $request->passport;
        $student->department_id = $request->department_id;
        $student->faculty_id = $request->faculty_id;
        $student->batch_id = $request->batch_id;
        $student->status = $request->status;
        $student->dateregistered = date('Y-m-d');
        $student->user_id = Auth::user()->id ;
        $student->image = $new_img_name;


        if($student->save())
        {
            $student_id = $student->student_id;
            $username = $student->username;
            $password = $student->password;

            Roll::insert([
                'student_id' => $student_id, 
                'username'  =>  $request->username,
                'password'  =>  $request->password
                ]);
               // dump($request->all()); die;
        }
       // dd( $request->all());

        // $admission = $this->admissionRepository->create($input);

        Flash::success('Student saved successfully.');

        return redirect(route('admissions.index'));
    }

    /**
     * Display the specified Admission.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {

        $admission = $this->admissionRepository->find($id);

        if (empty($admission)) {
            Flash::error('Student not found');

            return redirect(route('admissions.index'));
        }

        return view('admissions.show')->with('admission', $admission);
    }

    /**
     * Show the form for editing the specified Admission.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $faculties = Faculty::all();
        $departments = Department::all();
        $batches = Batch::all();

        // $username = 'SN-'.date('Y').'-'.($student_id+1);
        // $password = strtoupper(Str::random(3)).mt_rand(10, 99).strtoupper(Str::random(3));
        
        $admission = $this->admissionRepository->find($id);

        if (empty($admission)) {
            Flash::error('Student not found');

            return redirect(route('admissions.index'));
        }

        return view('admissions.edit',compact('faculties','departments','batches'))->with('admission', $admission);
    }

    /**
     * Update the specified Admission in storage.
     *
     * @param int $id
     * @param UpdateAdmissionRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        // $admission = $this->admissionRepository->find($id);
        // $input = $request->all();

        $file = $request->file('image');
        $extension = $file->getClientOriginalExtension();
        $new_img_name = $request->lname.'_'.time().'.'.$extension;
        $file->move(public_path('student_images'),$new_img_name);

        $student = array(
           'fname' => $request->fname,
           'lname' => strtoupper($request->lname),
           'father_name' => $request->father_name,
           'father_phone' => $request->father_phone,
           'mother_name' => $request->mother_name,
           'gender' => $request->gender,
           'email' => $request->email,
           'dob' => $request->dob,
           'phone' => $request->phone,
           'address' => $request->address,
           'current_address' => $request->current_address,
           'nationality' => $request->nationality,
           'passport' => $request->passport,
           'department_id' => $request->department_id,
           'faculty_id' => $request->faculty_id,
           'batch_id' => $request->batch_id,
           'status' => $request->status,
           'dateregistered' => date('Y-m-d'),
           'user_id' => Auth::user()->id ,
           'image' => $new_img_name
        );
        

        // var_dump($student);die;
        // echo "<pre>"; print_r($student);die;


        if (empty($student)) {
            Flash::error('Student not found');

            return redirect(route('admissions.index'));
        }

        Admission::findOrfail($id)->update($student);
        // $admission = $this->admissionRepository->update($request->all(), $id);

        Flash::success('Student updated successfully.');

        return redirect(route('admissions.index'));
    }

    /**
     * Remove the specified Admission from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $admission = $this->admissionRepository->find($id);

        if (empty($admission)) {
            Flash::error('Student not found');

            return redirect(route('admissions.index'));
        }

        $this->admissionRepository->delete($id);

        Flash::success('Student deleted successfully.');

        return redirect(route('admissions.index'));
    }
}
