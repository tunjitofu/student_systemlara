<?php

namespace App\Repositories;

use App\Models\Statuses;
use App\Repositories\BaseRepository;

/**
 * Class StatusesRepository
 * @package App\Repositories
 * @version September 11, 2020, 2:49 am UTC
*/

class StatusesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'teacher_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Statuses::class;
    }
}
