<?php

namespace App\Repositories;

use App\Models\Levels;
use App\Repositories\BaseRepository;

/**
 * Class LevelsRepository
 * @package App\Repositories
 * @version September 1, 2020, 2:02 pm UTC
*/

class LevelsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'level',
        'course_id',
        'level_description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Levels::class;
    }
}
