<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Semester
 * @package App\Models
 * @version September 3, 2020, 12:00 am UTC
 *
 * @property string $semesters_name
 * @property string $semesters_code
 * @property string $semesters_duration
 * @property string $semesters_description
 */
class Semester extends Model
{
    use SoftDeletes;

    public $table = 'semesters';

    protected $primaryKey = 'semesters_id';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'semesters_name',
        'semesters_code',
        'semesters_duration',
        'semesters_description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'semesters_id' => 'integer',
        'semesters_name' => 'string',
        'semesters_code' => 'string',
        'semesters_duration' => 'string',
        'semesters_description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'semesters_name' => 'required|string|max:255',
        'semesters_code' => 'required|string|max:255',
        'semesters_duration' => 'required|string|max:255',
        'semesters_description' => 'required|string',
        'deleted_at' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    
}
