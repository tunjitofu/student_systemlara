@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
           Edit Batch
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($batch, ['route' => ['batches.update', $batch->batch_id], 'method' => 'patch']) !!}

                        <div class="modal-body"><!-- Batch Field -->
                            <div class="form-group col-sm-12">
                                {!! Form::label('batch', 'Batch Year:') !!}
                                {!! Form::text('batch', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="{{ route('batches.index') }}" class="btn btn-default">Back</a>
                            {!! Form::submit('Update Batch', ['class' => 'btn btn-primary']) !!}
                        </div>
                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection