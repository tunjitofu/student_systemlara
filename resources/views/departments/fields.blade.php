<!-- Modal -->
<div class="modal fade" id="department-add-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Faculty Id Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('faculty_id', 'Faculty Id:') !!}
                    {{-- {!! Form::number('faculty_id', null, ['class' => 'form-control']) !!} --}}
                    <select name="faculty_id" id="faculty_id" class="form-control">
                        <option value="0"> Choose Faculty</option>
                        @foreach ($faculties as $key =>$faculty)
                            <option value="{{ $faculty->faculty_id }}">{{ $faculty->faculty_name }}</option>
                        @endforeach
                    </select>
                </div>

                <!-- Department Name Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('department_name', 'Department Name:') !!}
                    {!! Form::text('department_name', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
                </div>

                <!-- Department Code Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('department_code', 'Department Code:') !!}
                    {!! Form::text('department_code', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
                </div>

                <!-- Department Description Field --> 
                <div class="form-group col-sm-12 col-lg-12">
                    {!! Form::label('department_description', 'Department Description:') !!}
                    {!! Form::textarea('department_description', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group col-sm-6">
                    {!! Form::label('department_status', 'Department Status:') !!}
                    <label class="checkbox-inline">
                        {!! Form::hidden('department_status', 0) !!}
                        {!! Form::checkbox('department_status', '1', null) !!}
                    </label>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {!! Form::submit('Add Department', ['class' => 'btn btn-success']) !!}
            </div>
        </div>
    </div>
</div>