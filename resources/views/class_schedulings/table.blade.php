<div class="table-responsive">
    <table class="table" id="classSchedulings-table">
        <thead>
            <tr>
                <th>Course Id</th>
        <th>Class</th>
        <th>Level</th>
        {{-- <th>Shift</th>
        <th>Classroom</th>
        <th>Batch</th>
        <th>Day</th>
        <th>Time</th>
        <th>Semester</th>
        <th>Start Date</th>
        <th>End Date</th> --}}
        <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($classshedule as $classScheduling)
            <tr>
                <td>{{ $classScheduling->course_name }}</td>
                <td>{{ $classScheduling->class_name }}</td>
                <td>{{ $classScheduling->level }}</td>
                {{-- <td>{{ $classScheduling->shift }}</td>
                <td>{{ $classScheduling->classroom_name }}</td>
                <td>{{ $classScheduling->batch }}</td>
                <td>{{ $classScheduling->name }}</td>
                <td>{{ $classScheduling->time }}</td>
                <td>{{ $classScheduling->semesters_name }}</td>
                <td>{{ $classScheduling->show_start_date }}</td>
                <td>{{ $classScheduling->show_end_date }}</td> --}}
                <td>
                    @if ($classScheduling->status == 1)
                        {{-- <div class="btn btn-success btn-sm">Active</div> --}}
                        <i class="fa fa-check success-mine" aria-hidden="true"> Active</i>
                    @else
                        {{-- <div class="btn btn-danger btn-sm">In-Active</div> --}}
                        <i class="fa fa-times danger-mine" aria-hidden="true"> In-Active</i>
                    @endif
                </td>
                <td>
                    {!! Form::open(['route' => ['classSchedulings.destroy', $classScheduling->schedule_id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a data-toggle="modal" data-target="#schedule-view-modal" data-show_schedule_id="{{ $classScheduling->schedule_id }}" data-show_course_id="{{ $classScheduling->course_name }}"  data-show_class_id="{{ $classScheduling->class_name }}" data-show_level_id="{{ $classScheduling->level }}"  data-show_shift_id="{{ $classScheduling->shift }}"  data-show_classroom_id="{{ $classScheduling->classroom_name }}"  data-show_batch_id="{{ $classScheduling->batch }}"  data-show_day_id="{{ $classScheduling->name }}"  data-show_time_id="{{ $classScheduling->time }}"  data-show_semester_id="{{ $classScheduling->semesters_name }} - {{ $classScheduling->semesters_code }}"  data-show_start_date="{{ $classScheduling->start_date }}" data-show_end_date="{{ $classScheduling->end_date }}"  data-show_created_at="{{ $classScheduling->created_at }}" data-show_updated_at="{{ $classScheduling->updated_at }}"  
                            @if ($classScheduling->status == 1)
                            data-show_status="Active" 
                            @else
                                data-show_status="In-Active" 
                            @endif  
                        class='btn btn-default btn-xs margin-mine'><i class="glyphicon glyphicon-eye-open"></i> View Details</a>
                        
                        {{-- <a href="{{ route('classSchedulings.show', [$classScheduling->schedule_id]) }}" class='btn btn-default btn-xs margin-mine'><i class="glyphicon glyphicon-eye-open"></i></a> --}}

                        <a href="{{ route('classSchedulings.edit', [$classScheduling->schedule_id]) }}" class='btn btn-default btn-xs margin-mine'><i class="glyphicon glyphicon-edit"></i> Edit Details</a>

                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i> Delete Details', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<!--SHOW Modal -->
<div class="modal fade" id="schedule-view-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-calendar-check-o" aria-hidden="true"></i> Class Scheduling</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"><!-- Batch Field -->

                <input type="hidden" name="show_schedule_id" id="show_schedule_id">
               
                <!-- Class Id Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('class_id', 'Class Id:') !!}
                    {{-- {!! Form::number('class_id', null, ['class' => 'form-control']) !!} --}}
                    <input type="text" name="show_class_id" id="show_class_id" readonly>
                </div>

                <!-- Course Id Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('show_course_id', 'Course Id:') !!}
                    {{-- {!! Form::number('show_course_id', null, ['class' => 'form-control']) !!} --}}
                    <input type="text" size="100" name="show_course_id" id="show_course_id" readonly>

                </div>

                <!-- Level Id Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('show_level_id', 'Level Id:') !!}
                    {{-- {!! Form::number('show_level_id', null, ['class' => 'form-control']) !!} --}}
                    <input type="text" name="show_level_id" id="show_level_id" readonly>
                </div>

                <!-- Shift Id Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('show_shift_id', 'Shift Id:') !!}
                    {{-- {!! Form::number('show_shift_id', null, ['class' => 'form-control']) !!} --}}
                    <input type="text" name="show_shift_id" id="show_shift_id" readonly>

                </div>

                <!-- Classroom Id Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('show_classroom_id', 'Classroom Id:') !!}
                    {{-- {!! Form::number('show_classroom_id', null, ['class' => 'form-control']) !!} --}}
                    <input type="text" name="show_classroom_id" id="show_classroom_id" readonly>
                </div>

                <!-- Batch Id Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('show_batch_id', 'Batch Id:') !!}
                    {{-- {!! Form::number('show_batch_id', null, ['class' => 'form-control']) !!} --}}
                    <input type="text" name="show_batch_id" id="show_batch_id" readonly>
                </div>

                <!-- Day Id Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('show_day_id', 'Day Id:') !!}
                    {{-- {!! Form::number('show_day_id', null, ['class' => 'form-control']) !!} --}}
                    <input type="text" name="show_day_id" id="show_day_id" readonly>
                </div>

                <!-- Time Id Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('show_time_id', 'Time Id:') !!}
                    {{-- {!! Form::number('show_time_id', null, ['class' => 'form-control']) !!} --}}
                    <input type="text" name="show_time_id" id="show_time_id" readonly>
                </div>

                <!-- Semester Id Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('show_semester_id', 'Semester Id:') !!}
                    {{-- {!! Form::number('show_semester_id', null, ['class' => 'form-control']) !!} --}}
                    <input type="text" name="show_semester_id" id="show_semester_id" readonly>
                </div>

                <!-- Start Date Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('show_start_date', 'Start Date:') !!}
                    <input type="text" name="show_start_date" id="show_start_date" readonly>
                </div>

                <!-- End Date Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('show_end_date', 'End Date:') !!}
                    <input type="text" name="show_end_date" id="show_end_date" readonly>
                </div>

                <!-- show_status Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('show_status', 'Status:') !!}
                    <input type="text" name="show_status" id="show_status" readonly>
                </div>

                <!-- Created At Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('created_at', 'Created At:') !!}
                    <input type="text" name="show_created_at" id="show_created_at" readonly>
                </div>

                <!-- Updated At Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('updated_at', 'Updated At:') !!}
                    <input type="text" name="show_updated_at" id="show_updated_at" readonly>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@section('script')
    <script>
        $('#schedule-view-modal').on('show.bs.modal',function (event) {
            var button = $(event.relatedTarget)
            var show_class_id = button.data('show_class_id')
            var show_course_id = button.data('show_course_id')
            var show_level_id = button.data('show_level_id')
            var show_shift_id = button.data('show_shift_id')
            var show_classroom_id = button.data('show_classroom_id')
            var show_batch_id = button.data('show_batch_id')
            var show_day_id = button.data('show_day_id')
            var show_time_id = button.data('show_time_id')
            var show_semester_id = button.data('show_semester_id')
            var show_start_date = button.data('show_start_date')
            var show_end_date = button.data('show_end_date')
            var show_status = button.data('show_status')
            var show_created_at = button.data('show_created_at')
            var show_updated_at = button.data('show_updated_at')
            var show_schedule_id = button.data('show_schedule_id')

            var modal = $(this)

            // modal.find('.modal-title').text('View Batch Information');
            modal.find('.modal-body #show_class_id').val(show_class_id);
            modal.find('.modal-body #show_course_id').val(show_course_id);
            modal.find('.modal-body #show_level_id').val(show_level_id);
            modal.find('.modal-body #show_shift_id').val(show_shift_id);
            modal.find('.modal-body #show_classroom_id').val(show_classroom_id);
            modal.find('.modal-body #show_batch_id').val(show_batch_id);
            modal.find('.modal-body #show_day_id').val(show_day_id);
            modal.find('.modal-body #show_time_id').val(show_time_id);
            modal.find('.modal-body #show_semester_id').val(show_semester_id);
            modal.find('.modal-body #show_start_date').val(show_start_date);
            modal.find('.modal-body #show_end_date').val(show_end_date);
            modal.find('.modal-body #show_status').val(show_status);
            modal.find('.modal-body #show_created_at').val(show_created_at);
            modal.find('.modal-body #show_updated_at').val(show_updated_at);
            modal.find('.modal-body #show_schedule_id').val(show_schedule_id);
        });
    </script>
@endsection

