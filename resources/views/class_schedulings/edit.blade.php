@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
           Edit Class Scheduling
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($classScheduling, ['route' => ['classSchedulings.update', $classScheduling->schedule_id], 'method' => 'patch']) !!}

                   <div class="modal-body">
                
                    <!-- Class Id Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('class_id', 'Class Id:') !!}
                        {{-- {!! Form::number('class_id', null, ['class' => 'form-control']) !!} --}}
                        <select class="form-control" name="class_id" id="class_id">
                            <option value="">Select Class</option>
                            @foreach ($class as $clasee)
                                {{-- <option value="{{ $clasee->class_id }}">{{ $clasee->class_name }}</option> --}}
                                <option value="{{ $clasee->class_id }}" {{ ( $clasee->class_id == $classScheduling->class_id) ? 'selected' : '' }}> 
                                    {{ $clasee->class_name }} 
                                </option>
                            @endforeach
                        </select> 
                    </div>
    
                    <!-- Course Id Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('course_id', 'Course Id:') !!}
                        {{-- {!! Form::number('course_id', null, ['class' => 'form-control']) !!} --}}
                        <select class="form-control" name="course_id" id="course_id">
                            <option value="">Select Course</option>
                            @foreach ($course as $coursee)
                                {{-- <option value="{{ $coursee->course_id }}">{{ $coursee->course_name }}</option> --}}
                                <option value="{{ $coursee->course_id }}" {{ ( $coursee->course_id == $classScheduling->course_id) ? 'selected' : '' }}> 
                                    {{ $coursee->course_name }} 
                                </option>
                            @endforeach
                        </select>
                    </div>
    
                    <!-- Level Id Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('level_id', 'Level Id:') !!}
                        {{-- {!! Form::number('level_id', null, ['class' => 'form-control']) !!} --}}
                        <select class="form-control" name="level_id" id="level_id">
                            <option value="">Select Level</option>
                            @foreach ($level as $lev)
                                <option value="{{ $lev->course_id }}" {{ ( $lev->id == $classScheduling->level_id) ? 'selected' : '' }}> 
                                    {{ $lev->level }} 
                                </option>
                            @endforeach
                        </select>
                    </div>
    
                    <!-- Shift Id Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('shift_id', 'Shift Id:') !!}
                        {{-- {!! Form::number('shift_id', null, ['class' => 'form-control']) !!} --}}
                        <select class="form-control" name="shift_id" id="shift_id">
                            <option value="">Select Shift</option>
                            @foreach ($shift as $shif)
                                {{-- <option value="{{ $shif->shift_id }}">{{ $shif->shift }}</option> --}}
                                <option value="{{ $shif->shift_id }}" {{ ( $shif->shift_id == $classScheduling->shift_id) ? 'selected' : '' }}> 
                                    {{ $shif->shift }} 
                                </option>
                            @endforeach
                        </select>
                    </div>
    
                    <!-- Classroom Id Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('classroom_id', 'Classroom Id:') !!}
                        {{-- {!! Form::number('classroom_id', null, ['class' => 'form-control']) !!} --}}
                        <select class="form-control" name="classroom_id" id="classroom_id">
                            <option value="">Select Classroom</option>
                            @foreach ($classrooms as $classr)
                                {{-- <option value="{{ $classr->classroom_id }}">{{ $classr->classroom_name }} - {{ $classr->classroom_code }}</option> --}}
                                <option value="{{ $classr->classroom_id }}" {{ ( $classr->classroom_id == $classScheduling->classroom_id) ? 'selected' : '' }}> 
                                    {{ $classr->classroom_name }} 
                                </option>
                            @endforeach
                        </select>
                    </div>
    
                    <!-- Batch Id Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('batch_id', 'Batch Id:') !!}
                        {{-- {!! Form::number('batch_id', null, ['class' => 'form-control']) !!} --}}
                        <select class="form-control" name="batch_id" id="batch_id">
                            <option value="">Select Batch</option>
                            @foreach ($batch as $batchh)
                                {{-- <option value="{{ $batchh->batch_id }}">{{ $batchh->batch }}</option> --}}
                                <option value="{{ $batchh->batch_id }}" {{ ( $batchh->batch_id == $classScheduling->batch_id) ? 'selected' : '' }}> 
                                    {{ $batchh->batch }} 
                                </option>
                            @endforeach
                        </select>
                    </div>
    
                    <!-- Day Id Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('day_id', 'Day Id:') !!}
                        {{-- {!! Form::number('day_id', null, ['class' => 'form-control']) !!} --}}
                        <select class="form-control" name="day_id" id="day_id">
                            <option value="">Select Day</option>
                            @foreach ($day as $dayy)
                                {{-- <option value="{{ $dayy->day_id }}">{{ $dayy->name }}</option> --}}
                                <option value="{{ $dayy->day_id }}" {{ ( $dayy->day_id == $classScheduling->day_id) ? 'selected' : '' }}> 
                                    {{ $dayy->name }} 
                                </option>
                            @endforeach
                        </select>
                    </div>
    
                    <!-- Time Id Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('time_id', 'Time Id:') !!}
                        {{-- {!! Form::number('time_id', null, ['class' => 'form-control']) !!} --}}
                        <select class="form-control" name="time_id" id="time_id">
                            <option value="">Select Time</option>
                            @foreach ($time as $timee)
                                {{-- <option value="{{ $timee->time_id }}">{{ $timee->time }}</option> --}}
                                <option value="{{ $timee->time_id }}" {{ ( $timee->time_id == $classScheduling->time_id) ? 'selected' : '' }}> 
                                    {{ $timee->time }} 
                                </option>
                            @endforeach
                        </select>
                    </div>
    
                    <!-- Semester Id Field -->
                    <div class="form-group col-sm-4">
                        {!! Form::label('semester_id', 'Semester Id:') !!}
                        {{-- {!! Form::number('semester_id', null, ['class' => 'form-control']) !!} --}}
                        <select class="form-control" name="semester_id" id="semester_id">
                            <option value="">Select Semester</option>
                            @foreach ($semester as $sem)
                                {{-- <option value="{{ $sem->semesters_id }}">{{ $sem->semesters_name }} - {{ $sem->semesters_code }}</option> --}}
                                <option value="{{ $sem->semesters_id }}" {{ ( $sem->semesters_id == $classScheduling->semester_id) ? 'selected' : '' }}> 
                                    {{ $sem->semesters_name }} - {{ $sem->semesters_code }} 
                                </option>
                            @endforeach
                        </select>
                    </div>
    
                    <!-- Start Date Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('start_date', 'Start Date:') !!}
                        <input type="text" name="start_date" class="form-control" id="start_date" value="{{ $classScheduling->start_date }}">
                        {{-- {!! Form::date('start_date', null, ['class' => 'form-control','id'=>'start_date']) !!} --}}
                    </div>
    
                    @push('scripts')
                        <script type="text/javascript">
                            $('#start_date').datetimepicker({
                                format: 'YYYY-MM-DD',
                                useCurrent: false
                            })
                        </script>
                    @endpush
    
                    <!-- Different Date Format Date Field -->
                    {{-- <div class="form-group col-sm-6">
                        {!! Form::label('end_date', 'End Date:') !!}
                        <input type="text" class="form-control" name="end_date" id="end_date">
                    </div>
    
                    @push('scripts')
                        <script type="text/javascript">
                            $('#end_date').datetimepicker({
                                format: 'YYYY-MM-DD',
                                useCurrent: false
                            })
                        </script>
                    @endpush --}}
    
                    <!-- End Date Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('end_date', 'End Date:') !!}
                        <input type="text" name="end_date" class="form-control" id="end_date" value="{{ $classScheduling->end_date }}">
                        {{-- {!! Form::date('end_date', null, ['class' => 'form-control','id'=>'end_date']) !!} --}}
                    </div>
    
                    @push('scripts')
                        <script type="text/javascript">
                            $('#end_date').datetimepicker({
                                format: 'YYYY-MM-DD',
                                useCurrent: false
                            })
                        </script>
                    @endpush
    
                    <!-- Status Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('status', 'Status:') !!}
                        <label class="checkbox-inline">
                            {!! Form::hidden('status', 0) !!}
                            {!! Form::checkbox('status', '1', null) !!}
                        </label>
                    </div>
    
                </div>
                <div class="modal-footer">
                    <a href="{{ route('classSchedulings.index') }}" class="btn btn-default">Cancel</a>
                    {!! Form::submit('Update Schedule', ['class' => 'btn btn-primary']) !!}
                </div>

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection

{{-- @section('script')
<script>
    $('#course_id').on('change',function(e){
       //console.log(e);
        var course_id = e.target.value;
        
        $('#level_id').empty();
        $.get('dynamicLevel?course_id=' + course_id, function(data){
            //console.log(data);
            $.each(data,function(index,lev){ 
                $('#level_id').append('<option value ="'+lev.id+'">'+lev.level+'</option>')
            });
        });
    });
</script>
@endsection --}}