<!-- Modal -->
<div class="modal fade" id="semester-add-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-gear" aria-hidden="true"> Add New Semester</i></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Semesters Name Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('semesters_name', 'Semesters Name:') !!}
                    {!! Form::text('semesters_name', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
                </div>

                <!-- Semesters Code Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('semesters_code', 'Semesters Code:') !!}
                    {!! Form::text('semesters_code', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
                </div>

                <!-- Semesters Duration Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('semesters_duration', 'Semesters Duration:') !!}
                    {!! Form::text('semesters_duration', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
                </div>

                <!-- Semesters Description Field -->
                <div class="form-group col-sm-12 col-lg-12">
                    {!! Form::label('semesters_description', 'Semesters Description:') !!}
                    {!! Form::textarea('semesters_description', null, ['class' => 'form-control']) !!}
                </div>
 
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {!! Form::submit('Save Semester', ['class' => 'btn btn-success']) !!}
            </div>
        </div>
    </div>
</div>