<div class="table-responsive">
    <table class="table" id="semesters-table">
        <thead>
            <tr>
                <th>Semesters Name</th>
        <th>Semesters Code</th>
        <th>Semesters Duration</th>
        <th>Semesters Description</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($semesters as $semester)
            <tr>
                <td>{{ $semester->semesters_name }}</td>
            <td>{{ $semester->semesters_code }}</td>
            <td>{{ $semester->semesters_duration }}</td>
            <td>{{ $semester->semesters_description }}</td>
                <td>
                    {!! Form::open(['route' => ['semesters.destroy', $semester->semesters_id], 'method' => 'delete']) !!}
                    <div class='btn-group'>

                        <a data-toggle="modal" data-target="#semester-view-modal" data-semesters_id="{{ $semester->semesters_id }}"  data-semesters_name="{{ $semester->semesters_name }}" data-semesters_code="{{ $semester->semesters_code }}" data-semesters_duration="{{ $semester->semesters_duration }}" data-semesters_description="{{ $semester->semesters_description }}" data-created_at="{{ $semester->created_at }}" data-updated_at="{{ $semester->updated_at }}"  class='btn btn-default btn-xs margin-mine'><i class="glyphicon glyphicon-eye-open"></i></a>
                        
                        <a href="{{ route('semesters.edit', [$semester->semesters_id]) }}" class='btn btn-default btn-xs margin-mine'><i class="glyphicon glyphicon-edit"></i></a>

                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<!--SHOW Modal -->
<div class="modal fade" id="semester-view-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-gear" aria-hidden="true"></i> View Semesters</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"><!-- Batch Field -->
                <input type="hidden" name="semesters_id" id="semesters_id">
               
                <!-- Semesters Name Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('semesters_name', 'Semesters Name:') !!}
                    <input type="text" name="semesters_name" id="semesters_name" readonly>
                </div>

                <!-- Semesters Code Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('semesters_code', 'Semesters Code:') !!}
                    <input type="text" name="semesters_code" id="semesters_code" readonly>
                </div>

                <!-- Semesters Duration Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('semesters_duration', 'Semesters Duration:') !!}
                    <input type="text" name="semesters_duration" id="semesters_duration" readonly>
                </div>

                <!-- Semesters Description Field -->
                <div class="form-group col-sm-12 col-lg-12">
                    {!! Form::label('semesters_description', 'Semesters Description:') !!}
                    <textarea name="semesters_description" id="semesters_description" readonly> </textarea>
                </div>

                <!-- Created At Field -->
                <div class="form-group">
                    {!! Form::label('created_at', 'Created At:') !!}
                    <input type="text" name="created_at" id="created_at" readonly>
                </div>

                <!-- Updated At Field -->
                <div class="form-group">
                    {!! Form::label('updated_at', 'Updated At:') !!}
                    <input type="text" name="updated_at" id="updated_at" readonly>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@section('script')
    <script>
        $('#semester-view-modal').on('show.bs.modal',function (event) {
            var button = $(event.relatedTarget)
            var semesters_name = button.data('semesters_name')
            var semesters_code = button.data('semesters_code')
            var semesters_duration = button.data('semesters_duration')
            var semesters_description = button.data('semesters_description')
            var created_at = button.data('created_at')
            var updated_at = button.data('updated_at')
            var semesters_id = button.data('semesters_id')

            var modal = $(this)

            // modal.find('.modal-title').text('View Batch Information');
            modal.find('.modal-body #semesters_name').val(semesters_name);
            modal.find('.modal-body #semesters_code').val(semesters_code);
            modal.find('.modal-body #semesters_duration').val(semesters_duration);
            modal.find('.modal-body #semesters_description').val(semesters_description);
            modal.find('.modal-body #created_at').val(created_at);
            modal.find('.modal-body #updated_at').val(updated_at);
            modal.find('.modal-body #semesters_id').val(semesters_id);
        });
    </script>
@endsection
