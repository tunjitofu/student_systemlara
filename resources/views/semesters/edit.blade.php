@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="fa fa-gear">
            Edit Semester
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($semester, ['route' => ['semesters.update', $semester->semesters_id], 'method' => 'patch']) !!}

                   <div class="modal-body">
                        <!-- Semesters Name Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::label('semesters_name', 'Semesters Name:') !!}
                            {!! Form::text('semesters_name', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
                        </div>
        
                        <!-- Semesters Code Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::label('semesters_code', 'Semesters Code:') !!}
                            {!! Form::text('semesters_code', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
                        </div>
        
                        <!-- Semesters Duration Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::label('semesters_duration', 'Semesters Duration:') !!}
                            {!! Form::text('semesters_duration', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
                        </div>
        
                        <!-- Semesters Description Field -->
                        <div class="form-group col-sm-12 col-lg-12">
                            {!! Form::label('semesters_description', 'Semesters Description:') !!}
                            {!! Form::textarea('semesters_description', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="{{ route('semesters.index') }}" class="btn btn-default">Back</a>
                        {!! Form::submit('Update Semester', ['class' => 'btn btn-success']) !!}
                    </div>

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection