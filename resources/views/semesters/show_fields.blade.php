<!-- Semesters Name Field -->
<div class="form-group">
    {!! Form::label('semesters_name', 'Semesters Name:') !!}
    <p>{{ $semester->semesters_name }}</p>
</div>

<!-- Semesters Code Field -->
<div class="form-group">
    {!! Form::label('semesters_code', 'Semesters Code:') !!}
    <p>{{ $semester->semesters_code }}</p>
</div>

<!-- Semesters Duration Field -->
<div class="form-group">
    {!! Form::label('semesters_duration', 'Semesters Duration:') !!}
    <p>{{ $semester->semesters_duration }}</p>
</div>

<!-- Semesters Description Field -->
<div class="form-group">
    {!! Form::label('semesters_description', 'Semesters Description:') !!}
    <p>{{ $semester->semesters_description }}</p>
</div>

