@extends('layouts.frontLayout.app')
@section('content')

    <section class="content">
  
        <div class="row">
          <div class="col-md-3">
  
            <!-- Profile Image -->
            <div class="box box-primary">
              <div class="box-body box-profile">
                <img class="profile-image profile-user-img img-responsive img-circle" src="{{ asset('student_images/'.$students->image) }}" width="60px" height="100px" alt="User profile picture" style="border-radius: 25%; width: 150px; height: 150px; vertical-align: middle;">
  
                <h3 class="profile-username text-center">{{ strtoupper($students->lname) }}, {{ $students->fname }} </h3>
  
                <p class="text-muted text-center">students</p>
  
                <ul class="list-group list-group-unbordered">
                  <li class="list-group-item">
                    <b>Followers</b> <a class="pull-right">1,322</a>
                  </li>
                  <li class="list-group-item">
                    <b>Following</b> <a class="pull-right">543</a>
                  </li>
                  <li class="list-group-item">
                    <b>Friends</b> <a class="pull-right">13,287</a>
                  </li>
                </ul>
  
                <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
  
            <!-- About Me Box -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">About Me</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <strong><i class="fa fa-book margin-r-5"></i> Education</strong>
  
                <p class="text-muted">
                  B.S. in Computer Science from the University of Tennessee at Knoxville
                </p>
  
                <hr>
  
                <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
  
                <p class="text-muted">Malibu, California</p>
  
                <hr>
  
                <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>
  
                <p>
                  <span class="label label-danger">UI Design</span>
                  <span class="label label-success">Coding</span>
                  <span class="label label-info">Javascript</span>
                  <span class="label label-warning">PHP</span>
                  <span class="label label-primary">Node.js</span>
                </p>
  
                <hr>
  
                <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>
  
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab">Students TimeTable</a></li>
                <li><a href="#timeline" data-toggle="tab">Full Detail</a></li>
                <li><a href="#settings" data-toggle="tab">Settings</a></li>
              </ul>
              <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    <div class="content">
                      @include('adminlte-templates::common.errors')
                      <div class="box box-primary">
                          <div class="box-body">
                            <h1>Class Time Table</h1>
                          </div>
                      </div>
                    </div>
                    
                  </div>
                  <!-- /.tab-pane -->
                  
                    <div class="tab-pane" id="timeline">
                      <section class="content-header">
                        <h3>
                            Student Biodata/Profile
                        </h3>
                      </section>
                      <div class="content">
                          @include('adminlte-templates::common.errors')
                          <div class="box box-primary">
                              <div class="box-body">
                                <!-- Fname Field -->
                                <div class="form-group col-sm-12">
                                  {!! Form::label('fname', 'Full Name:') !!}
                                <p> {{ strtoupper($students->lname) }}, {{ $students->fname }}</p>
                                </div>

                                <!-- Email Field -->
                                <div class="form-group col-sm-12">
                                        {!! Form::label('email', 'Email:') !!}
                                        <p>{{ $students->email }}</p>
                                </div>

                                <!-- Gender Field -->
                                <div class="form-group col-sm-6">
                                    {!! Form::label('gender', 'Gender:') !!}
                                    <p>
                                        @if ($students->gender == 0)
                                            {{ 'Male' }}
                                        @else
                                            {{ 'Female' }}
                                        @endif
                                    </p>
                                </div>

                                <!-- Status Field -->
                                <div class="form-group col-sm-6">
                                    {!! Form::label('status', 'Status:') !!}
                                    <p>
                                        @if ($students->status == 0)
                                            {{ 'Single' }}
                                        @else
                                            {{ 'Married' }}
                                        @endif
                                    </p>
                                </div>
                                

                                <!-- Dob Field -->
                                <div class="form-group col-sm-6">
                                    {!! Form::label('dob', 'Dob:') !!}
                                    <p>{{ $students->dob }}</p>
                                </div>

                                <!-- Phone Field -->
                                <div class="form-group col-sm-6">
                                    {!! Form::label('phone', 'Phone:') !!}
                                    <p>{{ $students->phone }}</p>
                                </div>

                                <!-- Nationality Field -->
                                <div class="form-group col-sm-6">
                                    {!! Form::label('nationality', 'Nationality:') !!}
                                    <p>{{ $students->nationality }}</p>
                                </div>

                                <!-- Passport Field -->
                                <div class="form-group col-sm-6">
                                    {!! Form::label('passport', 'Passport:') !!}
                                    <p>{{ $students->passport }}</p>
                                </div>

                                <!-- Address Field -->
                                <div class="form-group col-sm-12">
                                    {!! Form::label('address', 'Address:') !!}
                                    <p>{{ $students->address }}</p>
                                </div>

                                <!-- Dateregistered Field -->
                                <div class="form-group col-sm-6">
                                    {!! Form::label('dateregistered', 'Dateregistered:') !!}
                                    <p>{{ $students->dateregistered }}</p>
                                </div>

                                <!-- User Id Field -->
                                <div class="form-group col-sm-6">
                                    {!! Form::label('user_id', 'Registrar ID:') !!}
                                    <p>{{ $students->user_id }}</p>
                                </div>
                              </div>
                          </div>
                      </div>
                    </div>
                <!-- /.tab-pane -->
                    <div class="tab-pane" id="settings">
                      <section class="content-header">
                        <h3>
                            Change Password
                        </h3>
                      </section>
                      <div class="content">
                          @include('adminlte-templates::common.errors')
                          <div class="box box-primary">
                              <div class="box-body"><br><br>
                                <form class="form-horizontal" action="{{ url('/student-update-password') }}" method="POST">
                                  @csrf
                                  <input type="text" name="email" id="" value="{{ $students->email }}">
                                  <div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">Old Password</label>
                                    <div class="col-sm-10">
                                      <input type="password" class="form-control" name="old_password" id="old_password" placeholder="Old Password">
                                      <i class="input-icon" id="messageError"></i>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="inputEmail" class="col-sm-2 control-label">New Password</label>
                
                                    <div class="col-sm-10">
                                      <input type="password" class="form-control" name="new_password" id="new_password"  placeholder="New Passwprd">
                                    </div>
                                  </div>
                                  
                                  <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                      <button type="submit" class="btn btn-primary">Update Password</button>
                                    </div>
                                  </div>
                                </form>
                              </div>
                          </div>
                      </div>
                    </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
  
    </section>
      <!-- /.content -->
    {{-- </div> --}}
    <!-- /.content-wrapper -->
@endsection
@section('script')
    <script>
      $(document).ready(function(){
        $("#old_password").keyup(function(){
          var old_password = $("#old_password").val();
          // alert(old_password);
          $.ajax({
            type: 'get',
            url: '/verify-password',
            data: {old_password:old_password},
            success: function(response){
              if(response == "false"){
                $("#messageError").html("<font color='red'> Password Incorrect </font>");
              }
              else if(response == "true"){
                $("#messageError").html("<font color='green'> Password Correct </font>");
              }
            }
          });
        });
      });
    </script>
@endsection