<!-- Course Name Field -->
<div class="form-group">
    {!! Form::label('course_name', 'Course Name:') !!}
    <p>{{ $course->course_name }}</p>
</div>

<!-- Course Code Field -->
<div class="form-group">
    {!! Form::label('course_code', 'Course Code:') !!}
    <p>{{ $course->course_code }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $course->description }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    @if ($course->status == 1)
        {{-- <div class="btn btn-success btn-sm">Active</div> --}}
        <i class="fa fa-check-square-o success-mine" aria-hidden="true"> Active</i>
    @else
        {{-- <div class="btn btn-danger btn-sm">In-Active</div> --}}
        <i class="fa fa-times danger-mine" aria-hidden="true"> In-Active</i>
    @endif
    {{-- <p>{{ $course->status }}</p> --}}
</div>

