<!-- Modal -->
<div class="modal fade" id="level-add-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add New Level</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Level Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('level', 'Level:') !!}
                    {!! Form::text('level', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
                </div>
 
                <!-- Course Id Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('course_id', 'Course:') !!}
                    {{-- {!! Form::number('course_id', null, ['class' => 'form-control']) !!} --}}
                    <select class="form-control" name="course_id" id="course_id">
                        <option value="">Select Course</option>
                        @foreach ($course as $coursee)
                            <option value="{{ $coursee->course_id }}">{{ $coursee->course_name }}</option>
                        @endforeach
                    </select>
                </div>

                <!-- Level Description Field -->
                <div class="form-group col-sm-12 col-lg-12">
                    {!! Form::label('level_description', 'Level Description:') !!}
                    {!! Form::textarea('level_description', null, ['class' => 'form-control']) !!}
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {!! Form::submit('Save Level', ['class' => 'btn btn-success']) !!}
            </div>
        </div>
    </div>
</div>
