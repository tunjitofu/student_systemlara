<div class="table-responsive">
    <table class="table" id="levels-table">
        <thead>
            <tr>
                <th>Level</th>
        <th>Course</th>
        <th>Level Description</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($level as $lev)
            <tr>
                <td>{{ $lev->level }}</td>
            <td>{{ $lev->course_name }}</td> 
            <td>{{ $lev->level_description }}</td> 
                <td>
                    {!! Form::open(['route' => ['levels.destroy', $lev->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        
                        <a data-toggle="modal" data-target="#level-view-modal" data-level_id="{{ $lev->id }}"  data-level="{{ $lev->level }}" data-course_id="{{ $lev->course_name }}" data-level_description="{{ $lev->level_description }}" data-created_at="{{ $lev->created_at }}" data-updated_at="{{ $lev->updated_at }}"  class='btn btn-default btn-xs margin-mine'><i class="glyphicon glyphicon-eye-open"></i></a>
                        
                        <a href="{{ route('levels.edit', [$lev->id]) }}" class='btn btn-default btn-xs margin-mine'><i class="glyphicon glyphicon-edit"></i></a>
                        
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<!--SHOW Modal -->
<div class="modal fade" id="level-view-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-level-up" aria-hidden="true"></i> View Level</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"><!-- Batch Field -->

                <input type="hidden" name="level_id" id="level_id">
               
                <!-- Level Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('level', 'Level:') !!}
                    <input type="text" name="level" id="level" readonly>
                </div>
 
                <!-- Course Id Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('course_id', 'Course:') !!}
                    {{-- {!! Form::number('course_id', null, ['class' => 'form-control']) !!} --}}
                    <input type="text" size="100" name="course_id" id="course_id" readonly>
                </div>

                <!-- Level Description Field -->
                <div class="form-group col-sm-12 col-lg-12">
                    {!! Form::label('level_description', 'Level Description:') !!}
                    <input type="text" size="100" name="level_description" id="level_description" readonly>
                </div>

                <!-- Created At Field -->
                <div class="form-group">
                    {!! Form::label('created_at', 'Created At:') !!}
                    <input type="text" name="created_at" id="created_at" readonly>
                </div>

                <!-- Updated At Field -->
                <div class="form-group">
                    {!! Form::label('updated_at', 'Updated At:') !!}
                    <input type="text" name="updated_at" id="updated_at" readonly>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@section('script')
    <script>
        $('#level-view-modal').on('show.bs.modal',function (event) {
            var button = $(event.relatedTarget)
            var level = button.data('level')
            var course_id = button.data('course_id')
            var level_description = button.data('level_description')
            var created_at = button.data('created_at')
            var updated_at = button.data('updated_at')
            var level_id = button.data('level_id')

            var modal = $(this)

            // modal.find('.modal-title').text('View Batch Information');
            modal.find('.modal-body #level').val(level);
            modal.find('.modal-body #course_id').val(course_id);
            modal.find('.modal-body #level_description').val(level_description);
            modal.find('.modal-body #created_at').val(created_at);
            modal.find('.modal-body #updated_at').val(updated_at);
            modal.find('.modal-body #level_id').val(level_id);
        });
    </script>
@endsection
