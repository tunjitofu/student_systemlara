@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Levels
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($levels, ['route' => ['levels.update', $levels->id], 'method' => 'patch']) !!}

                   <div class="modal-body">
                    <!-- Level Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::label('level', 'Level:') !!}
                        {!! Form::text('level', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
                    </div>
     
                    <!-- Course Id Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::label('course_id', 'Course:') !!}
                        {{-- {!! Form::number('course_id', null, ['class' => 'form-control']) !!} --}}
                        <select class="form-control" name="course_id" id="course_id">
                            <option value="">Select Course</option>
                            @foreach ($course as $coursee)
                                <option value="{{ $coursee->course_id }}" {{ ( $coursee->course_id == $levels->course_id) ? 'selected' : '' }}> 
                                    {{ $coursee->course_name }} 
                                </option>
                            @endforeach
                        </select>
                    </div>
    
                    <!-- Level Description Field -->
                    <div class="form-group col-sm-12 col-lg-12">
                        {!! Form::label('level_description', 'Level Description:') !!}
                        {!! Form::textarea('level_description', null, ['class' => 'form-control']) !!}
                    </div>
    
                </div>
                <div class="modal-footer">
                    <a href="{{ route('levels.index') }}" class="btn btn-default">Cancel</a>
                    {!! Form::submit('Update Level', ['class' => 'btn btn-primary']) !!}
                </div>

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection