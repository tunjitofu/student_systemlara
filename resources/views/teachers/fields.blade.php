<!-- Modal -->
<div class="modal fade left" id="teacher-add-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 90%" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><li class = "fa fa-user"> Add New Teacher</li></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <legend><i class="fa fa-user"></i> Teacher Details</legend>
                            Teacher No: 
                        </h3>
                    </div>
                    <div class="panel-body">{{-- Panel Body Starts --}}
                        <div class="form-group col-sm-12">
                            <input type="hidden" name="user_id" id="user_id" value="{{ Auth::user()->id }}">
                            <input type="hidden" name="dateregistered" id="dateregistered" value="{{ date('Y-m-d') }}" required>
                        </div>
                    

                        <!-- Lname Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('lname', 'Surname:') !!}
                            {{-- {!! Form::text('lname', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!} --}}
                            <input type="text" name="lname" id="lname" class="form-control text-uppercase" placeholder="Enter Last Name" required>
                        </div>

                        <!-- Fname Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('fname', 'Other Names:') !!}
                            {{-- {!! Form::text('fname', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'placeholder' => 'Enter First Name']) !!} --}}
                            <input type="text" name="fname" id="fname" class="form-control text-capitalize" placeholder="Enter First Name" required>
                        </div>

                        <!-- Gender Field -->
                        <div class="form-group col-sm-6"> 
                            {{-- {!! Form::label('gender', 'Gender:') !!} --}}
                            {{-- {!! Form::text('gender', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!} --}}
                           <fieldset >
                               <legend style="font-size: 14px; font-weight: bold">Select Gender</legend>
                               <label><input type="radio" name="gender" id="gender" value="0"> Male </label>
                               <label><input type="radio" name="gender" id="gender" value="1"> Female </label>
                           </fieldset>
                        </div>

                       <!-- Marital Status Field -->
                       <div class="form-group col-sm-6">
                            {{-- {!! Form::label('status', 'Marital Status:') !!} --}}
                            {{-- <label class="checkbox-inline">
                                {!! Form::hidden('status', 0) !!}
                                {!! Form::checkbox('status', '1', null) !!}
                            </label> --}}
                            <fieldset >
                                <legend style="font-size: 14px; font-weight: bold">Marital Status</legend>
                                <label><input type="radio" name="status" id="status" value="0" checked required> Single </label>
                                <label><input type="radio" name="status" id="status" value="1" required> Married </label>
                            </fieldset>
                        </div>

                        <!-- Dob Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('dob', 'Dob:') !!}
                            {{-- {!! Form::date('dob', null, ['class' => 'form-control','id'=>'dob']) !!} --}}
                            <input type="text" name="dob" id="dob" class="form-control" placeholder="YYYY-MM-DD" required>
                        </div>


                        <!-- Passport Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('passport', 'Passport:') !!}
                            {{-- {!! Form::text('passport', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!} --}}
                            <input type="text" name="passport" id="passport" class="form-control text-capitalize" placeholder="Enter Passport Number" required>
                        </div>

                        <!-- Nationality Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('nationality', 'Nationality:') !!}
                            {{-- {!! Form::text('nationality', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!} --}}
                            <input type="text" name="nationality" id="nationality" class="form-control text-capitalize" placeholder="Enter Nationality" required>
                        </div>

                        <!-- Phone Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('phone', 'Phone:') !!}
                            {{-- {!! Form::text('phone', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!} --}}
                            <input type="text" name="phone" id="phone" class="form-control" placeholder="Enter Phone Number" required>
                        </div>

                         <!-- Email Field -->
                         <div class="form-group col-sm-6">
                            {!! Form::label('email', 'Email:') !!}
                            {{-- {!! Form::email('email', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!} --}}
                            <input type="text" name="email" id="email" class="form-control" placeholder="Enter Email Address" required>
                        </div>

                        <!-- Image Field -->
                        <div class="form-group col-sm-6">
                            {{-- {!! Form::label('image', 'Image:') !!} --}}
                            {{-- {!! Form::text('image', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!} --}}
                            <div class="image">
                                {!! Html::image('teacher_images/profile-teacher.png', null, ['class' => 'teacher-image', 'id' => 'showImage'] ) !!}
                                <input type="file" name="image" id="image" accept="image/x-png,image/png,image/jpg,image/jpeg">
                                <input type="button" name="browse_file" id="browse_file" class="form-control btn-choose" value="Choose">
                            </div>
                        </div>

                        <!-- Address Field -->
                        <div class="form-group col-sm-12 col-lg-12 fa fa-map-marker">
                            {!! Form::label('address', 'Address:') !!}
                            {{-- {!! Form::textarea('address', null, ['class' => 'form-control']) !!} --}}
                            <textarea name="address" id="address" cols="40" rows="4" class="form-control text-capitalize"></textarea>
                        </div>


                        <!-- Dateregistered Field -->
                        {{-- <div class="form-group col-sm-6">
                            {!! Form::label('dateregistered', 'Dateregistered:') !!}
                            {!! Form::date('dateregistered', null, ['class' => 'form-control','id'=>'dateregistered']) !!}
                        </div>

                        @push('scripts')
                            <script type="text/javascript">
                                $('#dateregistered').datetimepicker({
                                    format: 'YYYY-MM-DD HH:mm:ss',
                                    useCurrent: false
                                })
                            </script>
                        @endpush --}}

                        <!-- User Id Field -->
                        {{-- <div class="form-group col-sm-6">
                            {!! Form::label('user_id', 'User Id:') !!}
                            {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
                        </div> --}}

                    </div> {{-- Panel Body Ends --}}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {!! Form::submit('Register Teacher', ['class' => 'btn btn-success']) !!}
            </div>
        </div>
    </div>
</div>

@section('script')
    <script type="text/javascript">
        //------------DOB------------
        $('#dob').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })
        
        //----------BROWSER IMAGE ---------------
        $('#browse_file').on('click',function(){
            $('#image').click();
        })
        $('#image').on('change',function(e){
            showFile(this,'#showImage');
        })

        $('#dob').datetimepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd'
        })

        function showFile(fileInput, img, showName) {
            if(fileInput.files[0]){
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(img).attr('src', e.target.result);
                }
                reader.readAsDataURL(fileInput.files[0]);
            }
            $(showName).text(fileInput.files[0].name)
        };

    </script>
@endsection