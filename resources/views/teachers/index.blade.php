@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left fa fa-user"> Teachers</h1>
        <h1 class="pull-right">
           <a class="btn btn-success pull-right" style="margin-top: -10px;margin-bottom: 5px" data-toggle="modal" data-target="#teacher-add-modal"><i class="fa fa-plus-circle"> Add New Teacher</i></a>

        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')
        @include('adminlte-templates::common.errors')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                 
                @include('teachers.table')
                {{-- {!! Form::open(['route' => 'teachers.store']) !!} --}}
                <form action="{{ route('teachers.store') }}" method="POST" enctype="multipart/form-data"> 
                    @csrf
                
                    @include('teachers.fields')

                </form>
                {{-- {!! Form::close() !!} --}}

            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

