<!-- Teacher Id Field -->
{{-- <div class="form-group">
    {!! Form::label('teacher_id', 'Teacher Id:') !!}
    <p>{{ $classAssigning->teacher_id }}</p>
</div> --}}

<!-- Class Schedule Id Field -->
{{-- <div class="form-group">
    {!! Form::label('class_schedule_id', 'Class Schedule Id:') !!}
    <p>{{ $classAssigning->class_schedule_id }}</p>
</div> --}}

<!-- Modal -->
<div class="modal fade" id="assigning-show" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 80%" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table" id="classAssignings-table">
                    <thead>
                        <tr>
                            <td>Teacher</td>
                            <td>Semester</td>
                            <td>Courses</td>
                            <td>Details</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <input type="text" name="" id="class_assign_id">
                            <td> <span id="lname"> </span> <span id="fname"> </span> </td>
                            <td> <span id="semester_name"> </span> </td>
                            <td> <span id="course_name"> </span> </td>
                            <td> 
                                <span id="level"> </span> | <span id="time"> </span> |<br>
                                <span id="name"> </span> | <span id="class_name"> </span> |<br>
                                <span id="shift"> </span> | <span id="batch"> </span> |<br>
                                <span id="classroom_name"> </span> 
                                <span id="created_at"> </span> 
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            {{-- <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div> --}}
        </div>
    </div>
</div>