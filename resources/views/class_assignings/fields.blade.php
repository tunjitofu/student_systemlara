<!-- Teacher Id Field -->
{{-- <div class="form-group col-sm-6">
    {!! Form::label('teacher_id', 'Teacher Id:') !!}
    {!! Form::number('teacher_id', null, ['class' => 'form-control']) !!}
</div> --}}

<!-- Class Schedule Id Field -->
{{-- <div class="form-group col-sm-6">
    {!! Form::label('class_schedule_id', 'Class Schedule Id:') !!}
    {!! Form::number('class_schedule_id', null, ['class' => 'form-control']) !!}
</div> --}}

<!-- Submit Field -->
{{-- <div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('classAssignings.index') }}" class="btn btn-default">Cancel</a>
</div> --}}

<div class="input-group col-md-12">
    <select name="teacher_id" id="" class="form-control" style="width: 50%">
        <option value="0" selected disabled>--Select Teacher--</option>
        @foreach ($teacher as $teach)
        <option value="{{ $teach->teachers_id }}">{{ strtoupper($teach->lname) }}, {{ $teach->fname }}</option>
            
        @endforeach
    </select>
    <br>
    <br>
    <br>
    <div class="clearfix"></div>
</div>

@section('script')
    <script>
        $(document).on('click', '.show-modal', function(){
            $('.form-horizontal').show();
            $('#class_assign_id').text($(this).data('class_assign_id'));
            $('#fname').text($(this).data('fname'));
            $('#lname').text($(this).data('lname'));
            $('#course_name').text($(this).data('course_name'));
            $('#semester_name').text($(this).data('semester_name'));
            $('#level').text($(this).data('level'));
            $('#time').text($(this).data('time'));
            $('#shift').text($(this).data('shift'));
            $('#classroom_name').text($(this).data('classroom_name'));
            $('#batch').text($(this).data('batch'));
            $('#name').text($(this).data('name'));
            $('#class_name').text($(this).data('class_name'));
            $('#created_at').text($(this).data('created_at'));
            $('#assigning-show').modal('show');
        })
    </script>
@endsection
