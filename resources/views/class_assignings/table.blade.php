<!-- Generate Class Schedule Modal -->
<div class="modal fade" id="classschedule-show" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width: 90%" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-user" aria-hidden="true"> Generate a Class for Teacher</i></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route' => 'insert', 'id' => 'mult', 'method' => 'post')) !!}
                {{-- <form action="{{ route('classAssignings.store') }}" method="post"> --}}
                @csrf
                {{-- Include Teacher's Select Menu --}}
                @include('class_assignings.fields') 

                <div class="table-responsive">
                    <table class="table" id="classAssignings-table">
                        <thead style="font-weight: bold">
                            <td>Check</td>
                            <td>Course</td>
                            <td>Level</td>
                            <td>Shift</td>
                            <td>Class Room</td>
                            <td>Batch</td>
                            <td>Days</td>
                            <td>Time</td>
                            <td>Semester</td>
                            <td>Start Date</td>
                            <td>End Date</td>
                        </thead>
                        <tbody>
                            @foreach ($classSchedules as $classSchedule)
                                <tr>
                                    <td><input type="checkbox" name="multiclass[]" id="" value="{{ $classSchedule->schedule_id }}"></td>
                                    <td>{{ $classSchedule->course_name }}</td>
                                    <td>{{ $classSchedule->level }}</td>
                                    <td>{{ $classSchedule->shift }}</td>
                                    <td>{{ $classSchedule->classroom_name }}</td>
                                    <td>{{ $classSchedule->batch }}</td>
                                    <td>{{ $classSchedule->name }}</td>
                                    <td>{{ $classSchedule->time }}</td>
                                    <td>{{ $classSchedule->semesters_name }}</td>
                                    <td>{{ date('d-m-Y', strtotime($classSchedule->start_date)) }}</td>
                                    <td>{{ date('d-m-Y', strtotime($classSchedule->end_date)) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="modal-footer">
                {!! Form::submit('Generate Class Assign', ['class' => 'btn btn-primary']) !!}
                {!! Form::close() !!}
                {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button> --}}
            </div>
        </div>
    </div>
</div>



<div class="table-responsive">
    <table class="table" id="classAssignings-table">
        <thead>
            <tr>
                <th>Teacher</th>
                <th>Semester</th>
                <th>Course</th>
                <th>Details</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($classAssignings as $classAssigning)
            <tr>
                <td>{{ strtoupper($classAssigning->lname) }}, {{ $classAssigning->fname }}</td>
                <td>{{ $classAssigning->semesters_name }}</td>
                <td>{{ $classAssigning->course_name }}</td>
                <td>
                    {{ $classAssigning->level }} | {{ $classAssigning->time }} |
                    {{ $classAssigning->name }} | {{ $classAssigning->class_name }} |
                    {{ $classAssigning->shift }} | {{ $classAssigning->batch }} |
                    {{ $classAssigning->classroom_name }}
                </td>
                <td>
                    {!! Form::open(['route' => ['classAssignings.destroy', $classAssigning->class_assign_id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="#" class='show-modal btn btn-default btn-xs margin-mine' data-class_assign_id={{ $classAssigning->class_assign_id }} data-name={{ $classAssigning->name }} data-fname={{ $classAssigning->fname }} data-lname={{ $classAssigning->lname }} data-semester_name={{ $classAssigning->semesters_name }} data-shift={{ $classAssigning->shift }} data-level={{ $classAssigning->level }} data-time={{ $classAssigning->time }} data-classroom_name={{ $classAssigning->classroom_name }} data-class_name={{ $classAssigning->class_name }} data-batch={{ $classAssigning->batch }} data-course_name={{ $classAssigning->course_name }}  data-created_at={{ $classAssigning->created_at }}    ><i class="glyphicon glyphicon-eye-open"></i></a>

                        <a href="{{ route('classAssignings.edit', [$classAssigning->class_assign_id]) }}" class='btn btn-default btn-xs margin-mine'><i class="glyphicon glyphicon-edit"></i></a>

                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $classAssignings->links() }}
</div>
