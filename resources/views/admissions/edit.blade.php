@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Admission
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                <form action="{{ route('admissions.update',$admission->student_id) }}" method="POST" enctype="multipart/form-data"> 
                    @method('PATCH')
                    @csrf
                    {{-- {!! Form::model($admission, ['route' => ['admissions.update', $admission->student_id], 'method' => 'patch']) !!} --}}

                   <div class="modal-body">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <legend><i class="fa fa-user"></i> Admission Form</legend>
                                Student No: 
                            </h3>
                        </div>
                        <div class="panel-body">{{-- Panel Body Starts --}}
                            <div class="form-group col-sm-12">
                                {{-- <input type="text" name="user_id" id="user_id" value="{{ Auth::user()->id }}"> --}}
                                {{-- <input type="text" name="dateregistered" id="dateregistered" value="{{ date('Y-m-d') }}" required> --}}
                                {{-- <input type="text" name="username" id="username" value="{{ $username }}">
                                <input type="text" name="password" id="password" value="{{ $password }}"> --}}
                            </div>
                        
    
                            <!-- Lname Field -->
                            <div class="form-group col-sm-6">
                                {!! Form::label('lname', 'Surname:') !!}
                                {{-- {!! Form::text('lname', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!} --}}
                                <input type="text" name="lname" value="{{ $admission->lname }}" id="lname" class="form-control text-uppercase" placeholder="Enter Last Name" required>
                            </div>
    
                            <!-- Fname Field -->
                            <div class="form-group col-sm-6">
                                {!! Form::label('fname', 'Other Names:') !!}
                                {{-- {!! Form::text('fname', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'placeholder' => 'Enter First Name']) !!} --}}
                                <input type="text" name="fname" value="{{ $admission->fname }}" id="fname" class="form-control text-capitalize" placeholder="Enter First Name" required>
                            </div>
    
                            <!-- Gender Field -->
                            <div class="form-group col-sm-6"> 
                                {{-- {!! Form::label('gender', 'Gender:') !!} --}}
                                {{-- {!! Form::text('gender', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!} --}}
                               <fieldset >
                                   <legend style="font-size: 14px; font-weight: bold">Select Gender</legend>
                                   <label><input type="radio" name="gender" id="gender" value="0" {{ $admission->gender == 0 ? 'checked' : '' }} required> Male </label>
                                   <label><input type="radio" name="gender" id="gender" value="1" {{ $admission->gender == 1 ? 'checked' : '' }} required> Female </label>
                               </fieldset>
                            </div>
    
                           <!-- Marital Status Field -->
                           <div class="form-group col-sm-6">
                                {{-- {!! Form::label('status', 'Marital Status:') !!} --}}
                                {{-- <label class="checkbox-inline">
                                    {!! Form::hidden('status', 0) !!}
                                    {!! Form::checkbox('status', '1', null) !!}
                                </label> --}}
                                <fieldset >
                                    <legend style="font-size: 14px; font-weight: bold">Marital Status</legend>
                                    <label><input type="radio" name="status" id="status" value="0" {{ $admission->status == 0 ? 'checked' : '' }} required> Single </label>
                                    <label><input type="radio" name="status" id="status" value="1" {{ $admission->status == 1 ? 'checked' : '' }} required> Married </label>
                                </fieldset>
                            </div>
    
                            <!-- Dob Field -->
                            <div class="form-group col-sm-4">
                                {!! Form::label('dob', 'Dob:') !!}
                                {{-- {!! Form::date('dob', null, ['class' => 'form-control','id'=>'dob']) !!} --}}
                                <input type="text" name="dob" value="{{ $admission->dob }}" id="dob" class="form-control" placeholder="YYYY-MM-DD" required>
                            </div>
    
    
                            <!-- Passport Field -->
                            <div class="form-group col-sm-4">
                                {!! Form::label('passport', 'Passport:') !!}
                                {{-- {!! Form::text('passport', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!} --}}
                                <input type="text" name="passport" value="{{ $admission->passport }}"  id="passport" class="form-control text-capitalize" placeholder="Enter Passport Number" required>
                            </div>
    
                            <!-- Nationality Field -->
                            <div class="form-group col-sm-4">
                                {!! Form::label('nationality', 'Nationality:') !!}
                                {{-- {!! Form::text('nationality', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!} --}}
                                <input type="text" name="nationality" value="{{ $admission->nationality }}" id="nationality" class="form-control text-capitalize" placeholder="Enter Nationality" required>
                            </div>
    
                            <!-- Phone Field -->
                            <div class="form-group col-sm-4">
                                {!! Form::label('phone', 'Phone:') !!}
                                {{-- {!! Form::text('phone', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!} --}}
                                <input type="text" name="phone" value="{{ $admission->phone }}"  id="phone" class="form-control" placeholder="Enter Phone Number" required>
                            </div>
    
                            
    
                             <!-- Email Field -->
                             <div class="form-group col-sm-4">
                                {!! Form::label('email', 'Email:') !!}
                                {{-- {!! Form::email('email', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!} --}}
                                <input type="text" name="email" value="{{ $admission->email }}"  id="email" class="form-control" placeholder="Enter Email Address" required>
                            </div>
    
                            <!-- Faculty Field -->
                            <div class="form-group col-sm-4">
                                {!! Form::label('faculty_id', 'Faculty:') !!}
                                {{-- {!! Form::email('email', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!} --}}
                                <select name="faculty_id" id="faculty_id" class="form-control">
                                    <option value="0" selected disabled>Choose Faculty</option>
                                    @foreach ($faculties as $fac)
                                        <option value="{{ $fac->faculty_id }}" {{ $fac->faculty_id == $admission->faculty_id ? 'selected' : '' }}>{{ $fac->faculty_name }}</option>
                                    @endforeach
                                </select>
                            </div>
    
    
                            <!-- Deparmtent Field -->
                            <div class="form-group col-sm-4">
                                {!! Form::label('department_id', 'Department:') !!}
                                {{-- {!! Form::email('email', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!} --}}
                                <select name="department_id" id="department_id" class="form-control">
                                    <option value="0" selected disabled>Choose Department</option>
                                    @foreach ($departments as $dept)
                                        <option value="{{ $dept->department_id }}" {{ $dept->department_id == $admission->department_id ? 'selected' : '' }}>{{ $dept->department_name }}</option>
                                    @endforeach
                                </select>
                            </div>
    
                            <!-- Batch Field -->
                            <div class="form-group col-sm-4">
                                {!! Form::label('batch_id', 'Batch:') !!}
                                {{-- {!! Form::email('email', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!} --}}
                                <select name="batch_id" id="batch_id" class="form-control">
                                    <option value="0" selected disabled>Choose Batch</option>
                                    @foreach ($batches as $batch)
                                        <option value="{{ $batch->batch_id }}" {{ $batch->batch_id == $admission->batch_id ? 'selected' : '' }}>{{ $batch->batch }}</option>
                                    @endforeach
                                </select>
                            </div>
    
                            <!-- Image Field -->
                            <div class="form-group col-sm-4">
                                {{-- {!! Form::label('image', 'Image:') !!} --}}
                                {{-- {!! Form::text('image', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!} --}}
                                <div class="image">
                                    {!! Html::image('student_images/'.$admission->image, null, ['class' => 'teacher-image', 'id' => 'showImage'] ) !!}
                                    <input type="file" name="image" id="image" accept="image/x-png,image/png,image/jpg,image/jpeg">
                                    <input type="button" name="browse_file" id="browse_file" class="form-control btn-choose" value="Choose">
                                </div>
                            </div>
    
                            <!-- Address Field -->
                            <div class="form-group col-sm-8 col-lg-6 fa fa-map-marker">
                                {!! Form::label('address', 'Address:') !!}
                                {{-- {!! Form::textarea('address', null, ['class' => 'form-control']) !!} --}}
                                <textarea name="address" id="address" cols="40" rows="4" class="form-control text-capitalize"> {{ $admission->address }}</textarea>
                            </div>
    
                            <!-- Current Address Field -->
                            <div class="form-group col-sm-8 col-lg-6 fa fa-map-marker">
                                {!! Form::label('current_address', 'Current Address:') !!}
                                {{-- {!! Form::textarea('address', null, ['class' => 'form-control']) !!} --}}
                                <textarea name="current_address"  id="current_address" cols="40" rows="4" class="form-control text-capitalize">{{ $admission->current_address }}</textarea>
                            </div>
    
                            <!-- Father Name Field -->
                            <div class="form-group col-sm-4">
                                {!! Form::label('father_name', 'Father Name:') !!}
                                {{-- {!! Form::text('fname', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'placeholder' => 'Enter First Name']) !!} --}}
                                <input type="text" name="father_name" value="{{ $admission->father_name }}"  id="father_name" class="form-control text-capitalize" placeholder="Enter Father's Name" required>
                            </div>
    
                            <!-- Father Phone Field -->
                            <div class="form-group col-sm-4">
                                {!! Form::label('father_phone', 'Father Phone Number:') !!}
                                {{-- {!! Form::text('fname', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'placeholder' => 'Enter First Name']) !!} --}}
                                <input type="text" name="father_phone" value="{{ $admission->father_phone }}" id="father_phone" class="form-control text-capitalize" placeholder="Enter Father's Phone Number" required>
                            </div>
    
                            <!-- Father Name Field -->
                            <div class="form-group col-sm-4">
                                {!! Form::label('mother_name', 'Mother Name:') !!}
                                {{-- {!! Form::text('fname', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'placeholder' => 'Enter First Name']) !!} --}}
                                <input type="text" name="mother_name" value="{{ $admission->mother_name }}" id="mother_name" class="form-control text-capitalize" placeholder="Enter Mother's Name" required>
                            </div>
                            <!-- Dateregistered Field -->
                            {{-- <div class="form-group col-sm-6">
                                {!! Form::label('dateregistered', 'Dateregistered:') !!}
                                {!! Form::date('dateregistered', null, ['class' => 'form-control','id'=>'dateregistered']) !!}
                            </div>
    
                            @push('scripts')
                                <script type="text/javascript">
                                    $('#dateregistered').datetimepicker({
                                        format: 'YYYY-MM-DD HH:mm:ss',
                                        useCurrent: false
                                    })
                                </script>
                            @endpush --}}
    
                            <!-- User Id Field -->
                            {{-- <div class="form-group col-sm-6">
                                {!! Form::label('user_id', 'User Id:') !!}
                                {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
                            </div> --}}
    
                        </div> {{-- Panel Body Ends --}}
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="{{ route('admissions.index') }}" class="btn btn-default">Cancel</a>
                    {!! Form::submit('Update Student', ['class' => 'btn btn-primary']) !!}
                </div>

                   {{-- </{!! Form::close() !!} --}}
                </form>
               </div>
           </div>
       </div>
   </div>
@endsection

@section('script')
    <script type="text/javascript">
        //------------DOB------------
        $('#dob').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })
        
        //----------BROWSER IMAGE ---------------
        $('#browse_file').on('click',function(){
            $('#image').click();
        })
        $('#image').on('change',function(e){
            showFile(this,'#showImage');
        })

        $('#dob').datetimepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd'
        })

        function showFile(fileInput, img, showName) {
            if(fileInput.files[0]){
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(img).attr('src', e.target.result);
                }
                reader.readAsDataURL(fileInput.files[0]);
            }
            $(showName).text(fileInput.files[0].name)
        };

    </script>
@endsection