<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route for MiddleWare
Route::group(['middleware' => ['studentSession']], function () {
    Route::match(['get','post'], 'account','StudentController@account');
    Route::match(['get','post'], 'student-biodata','StudentController@studentBiodata');
    Route::match(['get','post'], 'student-choose-course','StudentController@studentChooseCourse');
    Route::match(['get','post'], 'student-lecture-calendar','StudentController@studentLectureCalendar');
    Route::match(['get','post'], 'student-lecture-activity','StudentController@studentLectureActivity');
    Route::match(['get','post'], 'student-exam-marks','StudentController@studentExamMarks');

    Route::match(['get','post'], 'verify-password','StudentController@verifyPassword');
    Route::match(['get','post'], 'student-update-password','StudentController@updatePassword');

});

//Route for Login
Route::get('/student','StudentController@studentLogin');
Route::get('/logout','StudentController@studentLogout');


Route::post('student-login', 'StudentController@LoginStudent');

//Route for Forgot Password
Route::get('student-forgot-password', 'StudentController@getForgotPassword');
Route::post('forgot-password', 'StudentController@ForgotPassword');


Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->middleware('verified');

// Auth::routes();

// Route::get('/home', 'HomeController@index');

Route::resource('classes', 'ClassesController');

Route::resource('classrooms', 'ClassroomsController');

Route::resource('levels', 'LevelsController');

Route::resource('batches', 'BatchController');

Route::resource('shifts', 'ShiftController');

Route::resource('courses', 'CourseController');

Route::resource('faculties', 'FacultyController');

Route::resource('times', 'TimeController');

Route::resource('attendances', 'AttendanceController');

Route::resource('academics', 'AcademicController');

Route::resource('days', 'DayController');

Route::resource('classAssignings', 'ClassAssigningController');

Route::resource('classSchedulings', 'ClassSchedulingController');

Route::resource('transactions', 'TransactionController');

Route::resource('admissions', 'AdmissionController');

Route::resource('teachers', 'TeacherController');

Route::resource('roles', 'RoleController');

Route::resource('users', 'UserController');

Route::resource('semesters', 'SemesterController');

Route::resource('classSchedulings', 'ClassSchedulingController');

//route for dynamic selection
Route::get('/dynamicLevel', [
    'as' => 'dynamicLevel',
    'uses' => 'ClassSchedulingController@dynamicLevel'
    ]);

Route::resource('departments', 'DepartmentController');

Route::post('/insert', array('as' => 'insert', 'uses' => 'ClassAssigningController@insert'));


Route::resource('statuses', 'StatusesController');

//Multiple Language
Route::get('locale/{locale}', 'StudentController@languages');



// EMPLOYEE ROUTE
Route::get('/emp', 'EmployeeController@showEmployees');
Route::get('/emp/pdf','EmployeeController@createPDF');